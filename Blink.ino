
//Hecho por Federico Capdeville
//Probado con el Blue Pill (STM32F103C8T6)

//Hago titilar un led cada 1 segundo

#define LED PC13

void setup() 
{
  pinMode(LED, OUTPUT);      //Coloco un led en la pata PB12
}

void loop() 
{
   digitalWrite(LED, LOW); 
   delay(200); 
   digitalWrite(LED, HIGH);
   delay(200);
}
